const record = document.getElementById('record')
const stop = document.getElementById('stop')
const viewport = document.getElementById('viewport')

let audioContext = null
const canvasContext = viewport.getContext('2d')

let drawId = null

const resize = () => {
    const width = window.innerWidth
    const height = window.innerHeight
    viewport.width = width
    viewport.height = height
}
window.addEventListener('resize', resize)
resize()

record.addEventListener('click', () => {
    navigator.mediaDevices.getUserMedia({ audio: true })
        .then((stream) => {
            if (!audioContext) {
                audioContext = new AudioContext()
            }

            const analyzer = audioContext.createAnalyser()

            const source = audioContext.createMediaStreamSource(stream)
            source.connect(analyzer)

            analyzer.fftSize = 1024
            const wave = new Uint8Array(analyzer.frequencyBinCount)

            const draw = () => {
                const width = viewport.width
                const height = viewport.height
                canvasContext.clearRect(0, 0, width, height)

                drawId = requestAnimationFrame(draw)
                analyzer.getByteTimeDomainData(wave)

                canvasContext.lineWidth = 2
                canvasContext.strokeStyle = 'rgb(0, 0, 0)'
                canvasContext.shadowBlur = 10
                canvasContext.shadowColor = '#4CAF50'
                canvasContext.beginPath()

                canvasContext.moveTo(0, height * 0.5)

                const segmentWidth = width * 1.0 / wave.length
                for (let i = 0, x = 0; i < wave.length; i++, x += segmentWidth) {
                    const normalizedWaveHeight = wave[i] / 128.0
                    const y = normalizedWaveHeight * height * 0.5
                    canvasContext.lineTo(x, y)
                }

                canvasContext.lineTo(width, height * 0.5)
                canvasContext.stroke()
            }
            if (drawId != null) {
                cancelAnimationFrame(drawId)
            }
            drawId = requestAnimationFrame(draw)

            const audio = new Audio()
            audio.srcObject = stream
            audio.play()

            stop.addEventListener('click', () => {
                if (drawId != null) {
                    cancelAnimationFrame(drawId)
                }
                canvasContext.clearRect(0, 0, viewport.width, viewport.height)
                audio.pause()
                for (const track of stream.getAudioTracks()) {
                    track.stop()
                }
            })
        })
})
