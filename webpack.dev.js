module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: 'dist',
        watchContentBase: true,
        compress: true,
        open: true,
        overlay: true
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                options: {
                    fix: true
                }
            }
        ]
    }
}
